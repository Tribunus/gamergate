### Need to update or make changes to the OP (or anything)?
1. Make an account on [Github](https://github.com/).
2. Submit an [Issue](https://github.com/GamerGateOP/GamerGateOP/issues/new) OR [fork the Repository](https://github.com/GamerGateOP/GamerGateOP/fork), make your changes, then submit a Pull Request.

### Things to note when editing the OP:
#### 8chan:
OP posts have a 5000 character limit, make sure edits do not exceed this limit:
http://www.charactercountonline.com/

OP posts have a 40 link limit [as of 2014-09-30](https://twitter.com/infinitechan/status/516911792103362560) (was 20 before). Please ensure that the OP does not exceed 40 links.

#### 4chan:
OP posts have a 2000 character limit and no link limit (URL shorteners trigger the spam filter, with the exception of v.gd links).

However, due to logistical difficulties on 4chan (many included terms causing auto-saging, mods quickly deleting #GamerGate OPs, and the increasing amount of content being impossible to fit within 2000 characters), assume that the OP will no longer be posted on 4chan. Therefore, the 2000 character limit is no longer in effect.

# Want to help?
Because GamerGate is so fast moving, a lot of work needs to be done to keep the repository up to date.  
If any new important events occur, and they aren't covered in the repository, you can [Submit an Issue](https://github.com/GamerGateOP/GamerGateOP/issues/new) about it.    
Additionally, check out the [Issues](https://github.com/GamerGateOP/GamerGateOP/issues) for things that you can do to help.

### Want to discuss the OP or anything else GitHub related?
Hop onto the current IRC (#4free @ rizon SSL, port is 6697 if desired) or [Gitter](https://gitter.im/GamerGateOP/GamerGateOP).
